import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  StatusBar,
  ActivityIndicator,
} from 'react-native';

const base_url = 'http://demostand.parkinglight.io/';

const darkColor = 'black';
const whiteColor = 'white';
const blueColor = 'dodgerblue';
const redColor = 'red';
const lightContent = 'light-content';
const darkContent = 'dark-content';

const background = 'background';
const content = 'content';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: false,
      loading: false,
      init: false,
    };
  }

  componentDidMount(): void {
    this.getStatus();
  }

  setActions = () => {
    if (!this.state.loading) {
      this.setState({loading: true});
      fetch(`${base_url}ajax_action/`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        },
        body: `btn=${
          this.state.status ? 'false_switch' : 'true_switch'
        }&slider=100`,
      }).then(() => {
        this.setState({status: !this.state.status, loading: false});
      });
    }
  };

  getStatus = () => {
    fetch(`${base_url}get_lamp_status/`, {
      method: 'GET',
    })
      .then(response => {
        return response.json();
      })
      .then(({status}) => {
        this.setState({status, init: true});
      });
  };

  getColor = param => {
    const status = this.state.status;
    switch (param) {
      case 'background':
        return status ? whiteColor : darkColor;
      case 'content':
        return status ? darkContent : lightContent;
    }
  };

  render() {
    return (
      <View
        style={[
          styles.container,
          {backgroundColor: this.getColor(background)},
        ]}>
        <StatusBar
          backgroundColor={this.getColor(background)}
          barStyle={this.getColor(content)}
        />
        <View
          style={[
            styles.container1,
            {backgroundColor: this.getColor(background)},
          ]}>
          {this.state.init ? (
            <>
              <View style={styles.loader}>
                {this.state.loading ? (
                  <ActivityIndicator size="large" color={redColor} />
                ) : (
                  <></>
                )}
              </View>
              <TouchableOpacity
                style={styles.buttonContainer}
                onPress={() => this.setActions()}>
                <Image
                  style={[styles.imageStile, styles.onButton]}
                  source={require('./resources/icons/power-off.png')}
                />
              </TouchableOpacity>
            </>
          ) : (
            <ActivityIndicator size="large" color={blueColor} />
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loader: {
    top: 0,
    margin: 20,
    alignItems: 'center',
    position: 'absolute',
    justifyContent: 'center',
    width: '100%',
  },
  container1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageStile: {
    width: 200,
    height: 200,
  },
  inputIcon: {
    width: 30,
    height: 30,
    marginLeft: 15,
    justifyContent: 'center',
  },
  buttonContainer: {
    height: 200,
    width: 200,
    borderRadius: 200,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
  },
  onButton: {
    tintColor: blueColor,
  },
  loginText: {
    color: 'white',
  },
});
